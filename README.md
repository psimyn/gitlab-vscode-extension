# GitLab VSCode Extension

GitLab integration for VSCode.

### Features

Current version can open issues and MRs assigned to you. This is just the beginning and more to come.

### Setup

Extension needs your GitLab `userId`. set in the User Settings file. You can simply add it with the line below.

`"gitlab.userId": "GITLAB_USER_ID",`

If you are using GitLab on a custom domain, you should also set `gitlab.instanceUrl`. Default is `https://gitlab.com`.

You can open User Settings file pressing `Cmd+,` on Mac OS or go to `Code > Preferences > User Settings`.

### Usage

- Open up Command Palette by pressing `Cmd+Shift+P`.
- Search for GitLab

![https://gitlab.com/fatihacet/gitlab-vscode-extension/raw/master/src/assets/gitlab-vscode.png](https://gitlab.com/fatihacet/gitlab-vscode-extension/raw/master/src/assets/gitlab-vscode.png)
